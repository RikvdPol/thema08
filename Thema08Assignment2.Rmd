---
title: "Assignment 2"
author: "Rik van de Pol & Hans Zijlstra"
date: "May 9, 2019"
output: pdf_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

# Opdracht 1

Glucocorticoids zijn de meest effectieve anti-ontstekings therapie voor asthma, maar zijn relatief ineffectief bij de behandeling van chronic obstructive pulmonary disease(COPD).
Ze onderdrukken onstekingen door gebruik te maken van diverse moleculaire mechanismen.

Glucocorticoids onderdrukken dit door histon acetylatie of door geactiveerde onstekingsgenen door het binden van ligand-bound glucocorticoid receptors (GR) aan co-activator moleculen en de werving van histone deacetylase-2 aan het geactiveerde onstekings gen transcriptie complex(trans-repression)
[1]

# Opdracht 2

Bij toediening van het geneesmiddel (MPL) bindt het medicijn aan de receptor waardoor het MPL-receptor complex wordt gevormd. Dit complex wordt in het cytosol gevormd en verplaatst zich naar de nucleus. In de nucleus bindt dit complex zich aan het DNA waardoor er minder vrije receptor wordt aangemaakt. Daarnaast wordt het complex ook afgebroken in de nucleus. 

In de tabel hieronder weergegeven worden de variabele beschreven die worden gebruikt in de vergelijkingen van glucocorticoide receptor dynamica

Bij toediening van het geneesmiddel (MPL) bindt het medicijn aan de receptor waardoor het MPL-receptor complex wordt gevormd. Dit complex wordt in het cytosol gevormd en verplaatst zich naar de nucleus. In de nucleus bindt dit complex zich aan het DNA waardoor er minder vrije receptor wordt aangemaakt. Daarnaast wordt het complex ook afgebroken in de nucleus. 

In de tabel hieronder weergegeven worden de variabele beschreven die worden gebruikt in de vergelijkingen van glucocorticoide receptor dynamica


**Tabel 1**: De aanwezige parameters en hun betekenissen.

Parameters    | Betekenis
--------------|---------------------------------------
$D$           | De plasma concentratie van MPL in molair.
$mRRNA$       | De hoeveelheid receptor mRNA.
$R$           | De vrije glucocorticoid receptor dichtheid in het cytosol.
$k_{s\_Rm}$   | De nulde orde snelheidsconstante voor GR mRNA synthese.
$k_{d\_Rm}$   | De eerste orde snelheidsconstante voor GR mRNA afbraak.
$k_{s\_R}$    | De eerste orde snelheidsconstanten voor aanmaak van de receptor.
$k_{d\_R}$    | De eerste orde snelheidsconstanten voor afbraak receptor. 
$k_T$         | De eerste orde snelheidsconstante voor translocatie van het MPL-receptor complex naar de     nucleus.
$k_{re}$      | De eerste orde snelheidsconstante van de receptor van de celkern naar het cytosol.
$k_{on}$      | De tweede orde snelheidsconstante voor vorming van het MPL-receptor complex.
$R_f$         | De fractie vrije receptor die gerecycled word.
$IC_{50\_Rm}$ | De concentratie DR(N) waarbij de aanmaak van receptor mRNA daalt tot 50% van de basis waarde.


**Tabel 2**: De aanwezige variabelen en hun betekenissen.

Variabelen    | Betekenis
--------------|------------------
$R_{m0}$      | Basisniveau concentratie receptor mRNA.
$R_0$         | Basisniveau concentratie vrije receptor.
$DR$          | De dichtheid van het MPL-receptor complex in de celkern.
$DR(N)$       | De hoeveelheid MPL-receptor complex in de celkern.

[2]

# Opdracht 3 en 4
```{r}
library(deSolve)

# convertion to nmol
M <- 374.471
m <- 20.0
calc.n <- function(m, M){
  n <- m/M
}
mole <- calc.n(m,M)
D <- mole * 1000


parms <- c(ks_Rm = 2.90, IC50_Rm = 26.2,
kon = 0.00329,
kT = 0.63,
kre = 0.57,
Rf = 0.49,
kd_R = 0.0572,
kd_Rm = 0.612,
ks_r = 3.22,
D = D) 

state <- c(Rm0 = 4.74,
  R = 267,
  DR = 0,
  DRN = 0 )

#time in hours, two days
times <- seq(0,48, by = 1)

gluc_rep <- function(t, state, parms) {
  Rm0 <- state[1]         
  R <- state[2]         
  DR <- state[3]           
  DRN <- state[4]
 with(as.list(parms),
  {
     dRm <- ks_Rm*(1-DRN/(IC50_Rm  + DRN)) - kd_Rm*Rm0
     dR <- ks_r*Rm0 + Rf*kre*DRN - kon*D*R - kd_R*R
     dDR <- kon*D*R - kT*DR
     dDRN <- kT*DR - kre*DRN
 
     # store results in res
     res<-c(dRm,dR,dDR,dDRN)
     list(res)
  })
}

out1 <- ode(times = times, 
            y = state, 
            parms = parms, 
            func = gluc_rep, 
            method = "euler")

plot(out1, 
     xlab = "tijd in uren", 
     ylab = c("receptor mRNA", 
              "vrije glucocorticoid", 
              "dichtheid MPL-receptor complex", 
              "hoeveelheid MPL-receptor"), 
     main = c("Expressie van receptor mRNA"))

```

# Opgave 5

In de eerste uren neemt de hoeveelheid receptor mRNA af in de cel. De reden hiervoor is dat MPL zich bindt aan vrije receptor coticosteroide dat een MPL-receptor complex vormt. Dit zorgt ervoor dat er minder vrije receptor beschikbaar is waar MPL zich aan kan binden. Het gevormde MPL-receptor complex verplaatst zich naar de nucleus en bindt aan het DNA waardoor er verminderde productie is van vrije receptor coticosteroide. Door afwezigheid van vrije receptor coticosteroide wordt er geen MPL-receptor complex gevormd. Na verloop van tijd wordt het MPL-receptor complex gebonden aan het DNA afgebroken waardoor vrije receptor coticosteroide weer wordt aangemaakt. 

Het gevormde MPL-receptor complex is een belangrijke variabele in het model. De hoeveelheid MPL-receptor complex heeft een directe invloed op de afbraak van het medicijn. De binding reguleert direct de aanmaak van het vrije receptor coticosteroide. Een verminderde productie hiervan zorgt ervoor dat er minder MPL-receptor complex wordt gevormd. Een vermindering van de hoeveelheid MPL-receptor complex heeft tot gevolg dat het medicijn zal worden afgebroken, wat resulteert in een verminderde concentratie van 
het MPL.


# Bronnen
[**1**]Barnes P. J. (2011). Glucocorticosteroids: current and future directions. Br. J. Pharmacol. 163 29�43. geraadpleegd via: https://www.ncbi.nlm.nih.gov/pubmed/21198556
[**2**] Hanze Hogeschool ,Theme 08: System Biology, geraadpleegd op 11-05-2019, geraadpleegd via: https://bioinf.nl/~fennaf/thema08/week2.html